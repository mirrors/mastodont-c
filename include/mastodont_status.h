/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef MASTODONT_STATUS
#define MASTODONT_STATUS
#include <time.h>
#include <cjson/cJSON.h>
#include "mastodont_pleroma.h"
#include "mastodont_types.h"
#include "mastodont_fetch.h"
#include "mastodont_attachment.h"
#include "mastodont_application.h"
#include "mastodont_mention.h"
#include "mastodont_account.h"
#include "mastodont_emoji.h"
#include "mastodont_tag.h"
#include "mastodont_pleroma.h"
#include "mastodont_account.h"
#include "mastodont_visibility_types.h"
/* Status: Complete, not implemented */

struct mstdnt_status
{
    char* id;
    char* uri;
    time_t created_at;
    struct mstdnt_account account;
    char* content;
    enum mstdnt_visibility_type visibility;
    mstdnt_bool sensitive;
    char* spoiler_text;
    struct mstdnt_attachment* media_attachments;
    size_t media_attachments_len;
    struct mstdnt_app* application;
    struct mstdnt_status_pleroma pleroma;

    /* Rendering attributes */
    struct mstdnt_mention* mentions;
    struct mstdnt_tag* tags;
    struct mstdnt_emoji* emojis;
    size_t emojis_len;

    /* Information attributes */
    unsigned reblogs_count;
    unsigned favourites_count;
    unsigned replies_count;

    /* Nullable attributes */
    char* url;
    char* in_reply_to_id;
    char* in_reply_to_account_id;
    struct mstdnt_status* reblog;
    struct mstdnt_poll* poll;
    struct mstdnt_card* card;
    char* language;
    char* text;

    /* Authorized user attributes */
    mstdnt_bool favourited;
    mstdnt_bool reblogged;
    mstdnt_bool muted;
    mstdnt_bool bookmarked;
    mstdnt_bool pinned;
};

struct mstdnt_account_statuses_args
{
    mstdnt_bool pinned;
    mstdnt_bool only_media;
    mstdnt_bool with_muted;
    mstdnt_bool exclude_reblogs;
    mstdnt_bool exclude_replies;
    char* tagged;
    char* max_id;
    char* min_id;
    char* since_id;
    int offset;
    int limit;
};

struct mstdnt_status_args
{
    char* in_reply_to_id;
    char* content_type;
    int expires_in;
    char* in_reply_to_conversation_id;
    char* language;
    char* poll; //?
    char* preview;
    char* scheduled_at;
    int sensitive;
    char* spoiler_text;
    char* status;
    char* visibility;
    char** media_ids;
    int media_ids_len;
};

// Cleanup
void mstdnt_cleanup_statuses(struct mstdnt_status* statuses, size_t s);
void mstdnt_cleanup_status(struct mstdnt_status* status);

int mstdnt_status_json(struct mstdnt_status* status, cJSON* js);
int mstdnt_status_emoji_react(mastodont_t* api,
                                 struct mstdnt_args* m_args,
                                 char* id,
                                 char* emoji,
                                 struct mstdnt_storage* storage,
                                 struct mstdnt_status* status);

// Generated function
int mstdnt_statuses_json(struct mstdnt_status* statuses[],
                         size_t* size,
                         cJSON* js);

void _mstdnt_val_status_call(cJSON* v, void* _type);
void _mstdnt_val_malloc_status_call(cJSON* v, void* _type);

int mstdnt_status_context_json(struct mstdnt_status* statuses_before[],
                               struct mstdnt_status* statuses_after[],
                               size_t* size_before,
                               size_t* size_after,
                               cJSON* js);

int mstdnt_get_account_statuses(mastodont_t* data,
                                   struct mstdnt_args* m_args,
                                   char* id,
                                   struct mstdnt_account_statuses_args* args,
                                   struct mstdnt_storage* storage,
                                   struct mstdnt_status* statuses[],
                                   size_t* size);

int mstdnt_get_status(mastodont_t* data,
                         struct mstdnt_args* m_args,
                         char* id,
                         struct mstdnt_storage* storage,
                         struct mstdnt_status* status);

int mstdnt_get_status_context(mastodont_t* data,
                                 struct mstdnt_args* m_args,
                                 char* id,
                                 struct mstdnt_storage* storage,
                                 struct mstdnt_status* statuses_before[],
                                 struct mstdnt_status* statuses_after[],
                                 size_t* size_before,
                                 size_t* size_after);

int mstdnt_status_favourited_by(mastodont_t* data, struct mstdnt_args* args,
                                   char* id,
                                   struct mstdnt_storage* storage,
                                   struct mstdnt_account* accounts[],
                                   size_t* accts);

int mstdnt_status_reblogged_by(mastodont_t* data,
                                  struct mstdnt_args* m_args,
                                  char* id,
                                  struct mstdnt_storage* storage,
                                  struct mstdnt_account* accounts[],
                                  size_t* accts);

int mstdnt_create_status(mastodont_t* data,
                            struct mstdnt_args* m_args,
                            struct mstdnt_status_args* args,
                            struct mstdnt_storage* storage);

/* Generates do and undo functions */
#define MSTDNT_STATUS_ACTION_DECL(type) int mstdnt_##type##_status(mastodont_t* data, struct mstdnt_args* m_args, char* id, struct mstdnt_storage* storage, struct mstdnt_status* status)
#define MSTDNT_STATUS_ACTION_FUNC_URL(action) {                         \
        return mstdnt_status_action(data, m_args, id, storage, status, "api/v1/statuses/%s/" action); \
    }

MSTDNT_STATUS_ACTION_DECL(favourite);
MSTDNT_STATUS_ACTION_DECL(unfavourite);
MSTDNT_STATUS_ACTION_DECL(reblog);
MSTDNT_STATUS_ACTION_DECL(unreblog);
MSTDNT_STATUS_ACTION_DECL(pin);
MSTDNT_STATUS_ACTION_DECL(unpin);
MSTDNT_STATUS_ACTION_DECL(bookmark);
MSTDNT_STATUS_ACTION_DECL(unbookmark);
MSTDNT_STATUS_ACTION_DECL(delete);

int mstdnt_mute_conversation(mastodont_t* data,
                                struct mstdnt_args* m_args,
                                char* id,
                                struct mstdnt_storage* storage,
                                struct mstdnt_status* status);

int mstdnt_unmute_conversation(mastodont_t* data,
                                  struct mstdnt_args* m_args,
                                  char* id,
                                  struct mstdnt_storage* storage,
                                  struct mstdnt_status* status);

/* Callbacks */
struct _mstdnt_statuses_cb_args
{
    struct mstdnt_status** statuses;
    size_t* size;
};

struct _mstdnt_status_context_result_cb_args
{
    struct mstdnt_status** statuses_before;
    struct mstdnt_status** statuses_after;
    size_t* size_before;
    size_t* size_after;
};

struct mstdnt_bookmarks_args
{
    char* max_id;
    char* since_id;
    char* min_id;
    int limit;
};

struct mstdnt_favourites_args
{
    char* max_id;
    char* min_id;
    int limit;
};

int mstdnt_get_bookmarks(mastodont_t* data,
                            struct mstdnt_args* m_args,
                            struct mstdnt_bookmarks_args* args,
                            struct mstdnt_storage* storage,
                            struct mstdnt_status* statuses[],
                            size_t* size);

int mstdnt_get_favourites(mastodont_t* data,
                             struct mstdnt_args* m_args,
                             struct mstdnt_favourites_args* args,
                             struct mstdnt_storage* storage,
                             struct mstdnt_status* statuses[],
                             size_t* size);

int mstdnt_statuses_json_callback(cJSON* json, void* _args);
int mstdnt_status_json_callback(cJSON* json, void* status);
int mstdnt_status_context_json_callback(cJSON* json, void* _args);

#endif /* MASTODONT_STATUS */
